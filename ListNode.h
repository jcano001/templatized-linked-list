// Templatized ListNode
// Justin Cano
// May 25, 2014
#ifndef __NODE_H__
#define __NODE_H__

template <typename elementType>
struct ListNode
{
	elementType info;
	ListNode<elementType> * next;
	ListNode( elementType newInfo, ListNode * newNext )
	 :info( newInfo ), next( newNext ) {}

	ListNode operator = ( const ListNode * & l )
	{
		info = l->info;
		next = l->next;
		return *this;
	}
};

#endif
