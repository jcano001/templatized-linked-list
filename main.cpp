#include <iostream>
#include <fstream>
#include <ostream>
#include <vector>

using namespace std;

template <typename T>
void load( vector<T> & v, char* fileName )
{
	ifstream in( fileName );
	T input;
	while( in >> input )
		v.push_back(input);
	in.close();
}

template <typename T>
void print( vector<T> & v )
{
	for( int i = 0; i < v.size(); i++ )
		cout << v[i] << endl;
}

template <typename T>
void my_swap( T & a, T & b )
{
	T temp = a;
	a = b;
	b = temp;
}

template <typename T>
void bubbleSort( vector<T> & v )
{
	for( int i = 0; i < v.size(); i++ )
	{
		for( int j = 0; j < v.size()-1; j++ )
		{
			if( v[j] > v[j+1] )
				my_swap( v[j], v[j+1] );
		}
	}
}

int main()
{
	vector<string> v;

	load( v, "randomWords.txt" );

	print( v );

	cout << endl;
	bubbleSort( v );

	print( v );

	return 0;
}
