# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
SRC = $(shell ls *.cc)

SOURCES = main.cc
LINK = g++
CXX = g++
CXX_FLAGS = -c 


all: testLinkedList

testLinkedList:
	@echo making $@
	$(LINK) -ggdb -o $@ testLinkedList.cpp

clean:
	$(RM) -r *.o a.out testLinkedList testLinkedList.dSYM

tarball:
	tar -cvzf LinkedList.tgz *
