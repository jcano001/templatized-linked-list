#include <iostream>
#include <fstream>
#include <ostream>
#include "LinkedList.h"

using namespace std;

template <typename T>
void load( LinkedList<T> & ll, char* fileName )
{
	ifstream in( fileName );
	T input;
	while( in >> input )
		ll.PushBack(input);
	in.close();
}

int main()
{
	LinkedList<string> ll;
	//LinkedList<int> ll;

	load( ll, "randomWords.txt" );
	//load( ll, "intStream.txt" );

	ll.Print();

	cout << endl;

	ll.BubbleSort();
	//ll.Print();

	return 0;
}
