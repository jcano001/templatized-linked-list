// Templatized LinkedList
// Justin Cano
// May 25, 2014
#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__
#include <string>
#include "ListNode.h"
#include "Timer.h"

template struct ListNode<int>;
template struct ListNode<char>;
template struct ListNode<double>;
template struct ListNode<float>;
template struct ListNode<std::string>;

template <typename elementType>
class LinkedList
{
	private:
		int size;
		ListNode<elementType> * head;
		ListNode<elementType> * tail;
		ListNode<elementType> * Make( elementType info, ListNode<elementType> * next )
		{
			return new ListNode<elementType> ( info, next );
		}
		void Swap( ListNode<elementType> * & a, ListNode<elementType> * & b )
		{
			elementType temp = a->info;
			a->info = b->info;
			b->info = temp;
		}

	public:
		LinkedList()
 		 : size( 0 ), head( NULL ), tail( NULL ) {}
		~LinkedList()
		{
			for( ListNode<elementType> * p = head; p; )
				{
					ListNode<elementType> * q = p;
					p = p->next;
					delete q;
				}
		}

		void Print()
		{
			for( ListNode<elementType> * p = head; p; p = p->next )
			{
				std::cout << p->info << std::endl;
			}
		}

		void PushBack( elementType newInfo )
		{
			if( !head )
			{
				head = Make( newInfo, NULL );
				tail = head;
			}
			else
			{
				tail->next = Make( newInfo, tail->next );
				tail = tail->next;
			}
		}

		void BubbleSort()
		{
			std::cout << "Invoking BubbleSort()!\n";
			Timer t;
			double time;
			t.start();
			for( ListNode<elementType> * i = head; i; i = i->next )
			{
				for( ListNode<elementType> * j = head; j->next; j = j->next )
				{
					if( j->info > j->next->info )
						Swap( j, j->next );
				}
			}
			t.elapsedUserTime( time );
			std::cout << "Elapsed Time: " << time << "seconds\n";
		}
};

#endif
